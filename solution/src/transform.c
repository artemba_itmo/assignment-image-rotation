/* создаёт копию изображения, которая повёрнута на 90 градусов */
#include "util.h"
#include "transform.h"
#include <malloc.h>

static struct image rotated_img;// must be declared outside the function because it will be used as a return value after exit from the function

struct image rotate(struct image const source_img, const int32_t rotation_direction)
{
	rotated_img = source_img;
	rotated_img.data = NULL;
	if (alloc_image_buffer(&rotated_img) != OP_OK)//allocate image->data buffer here (image dimensions are known and checked)
	{
		rotated_img.data = NULL;
		return rotated_img;
	}

	uint64_t   x, y;
	int64_t base_dst_index, dst_index, src_index, DstSizeX, DstStartX, DstStartY, DstStepX, DstStepY;


	switch (rotation_direction)
	{
		case ROTATION_90CCW:
			rotated_img.height = source_img.width;
			rotated_img.width = source_img.height;

			DstSizeX = rotated_img.width;
			DstStartX = 0;
			DstStartY = rotated_img.height - 1;
			DstStepX = -DstSizeX;
			DstStepY = 1;
			break;

		case ROTATION_90CW:
			rotated_img.height = source_img.width;
			rotated_img.width = source_img.height;

			DstSizeX = rotated_img.width;
			DstStartX = DstSizeX - 1;
			DstStartY = 0;
			DstStepX = DstSizeX;
			DstStepY = -1;
			break;

		case ROTATION_180:
			DstSizeX = rotated_img.width;
			DstStartX = DstSizeX - 1;
			DstStartY = rotated_img.height - 1;
			DstStepX = -1;
			DstStepY = -DstSizeX;
			break;

		default:// ROTATION_NONE
			DstSizeX = rotated_img.width;
			DstStartX = 0;
			DstStartY = 0;
			DstStepX = 1;
			DstStepY = DstSizeX;
			break;
	}

	for (y = 0, base_dst_index = DstStartY*DstSizeX + DstStartX, src_index = 0; 
		y < source_img.height; 
		y++, base_dst_index += DstStepY)
	{
		for (x = 0, dst_index = base_dst_index; 
			x < source_img.width; 
			x++, dst_index = dst_index + DstStepX, src_index++)
		{
			*(rotated_img.data + dst_index) = *(source_img.data + src_index);
		}
	}

	return rotated_img;
}

int32_t alloc_image_buffer(struct image *imgptr)
{
	if (imgptr->data != NULL)
		return MEMORY_ALLOC_ERROR;

	imgptr->data = (struct pixel *) malloc((size_t)(imgptr->height*imgptr->width * sizeof(struct pixel)));
	if (imgptr->data == NULL)
		return MEMORY_ALLOC_ERROR;

	return OP_OK;
}

void  free_image_buffer(struct image *imgptr)
{
	if (imgptr->data != NULL)
	{
		free(imgptr->data);
		imgptr->data = NULL;
	}
}

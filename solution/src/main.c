#include <stdbool.h>
#include <stdlib.h>

#include "util.h"
#include "bmpfile.h"
#include "rawimage.h"
#include "transform.h"


int main(int argc, char **argv) 
{
	int32_t rc, rotation_direction;

	if (argc < 3 || argc > 4)
	{
		print_msg(stderr, "command line arguments format", "\n<source-image.bmp> <transformed-image.bmp> [<rotation-direction {0(NONE),1(90CCW),2(90CW),3(180)}>]",NULL);
		return 1;
	}
	if (argc == 4)
	{
		rotation_direction = atoi(argv[3]);
		if (rotation_direction != ROTATION_NONE && rotation_direction != ROTATION_90CCW && rotation_direction != ROTATION_90CW && rotation_direction != ROTATION_180)
		{
			print_msg(stderr, "Error in command line attribute <rotation-direction>", "Unknown value", NULL);
			return 1;
		}
	}
	else
		rotation_direction = ROTATION_90CCW; //default rotation mode

	struct image input_img;
	input_img.data = NULL; //image buffer is not yet allocated

	struct image transformed_img;

	struct bmp_header bmp_hdr;

	FILE *inputfile, *outputfile;

	if ((rc = open_binary_file_to_read(argv[1], &inputfile)) != OP_OK)
	{
		print_msg(stderr, "Error in open_binary_file_to_read", argv[1], &rc);
		return 1;  
	}

	if ((rc = from_bmp(inputfile, &bmp_hdr, &input_img)) != OP_OK)
	{
		fclose(inputfile);
		print_msg(stderr, "Error in from_bmp", NULL, &rc);
		return 1;
	}
	
	transformed_img = rotate(input_img,rotation_direction);
	if (transformed_img.data == NULL)
	{
		fclose(inputfile);
		free_image_buffer(&input_img);
		rc = MEMORY_ALLOC_ERROR;
		print_msg(stderr, "Error in rotate", NULL, &rc);
		return 1;
	}

	if( (rc = open_binary_file_to_write( argv[2], &outputfile)) != OP_OK)
	{
		fclose(inputfile);
		free_image_buffer(&input_img);
		free_image_buffer(&transformed_img);
		print_msg(stderr, "Error in open_binary_file_to_write", argv[2], &rc);
		return 1;
	}

	if ((rc = to_bmp(outputfile, &bmp_hdr, &transformed_img, (rotation_direction==ROTATION_90CCW || rotation_direction==ROTATION_90CW) )) != OP_OK)
	{
		fclose(inputfile);
		fclose(outputfile);
		free_image_buffer(&input_img);
		free_image_buffer(&transformed_img);
		print_msg(stderr, "Error in to_bmp", NULL, &rc);
		return 1;
	}

	print_msg(stdout, "done", NULL, NULL);

	fclose(inputfile);
	fclose(outputfile);
	free_image_buffer(&input_img);
	free_image_buffer(&transformed_img);
	return 0;
}

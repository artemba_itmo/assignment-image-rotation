#include "util.h"

enum operation_status open_binary_file_to_read( const char *filename, FILE** file ) {
#ifdef _MSC_VER  
	int32_t rc = fopen_s(file, filename, "rb"); // "rb - read and binary"
	if( rc == 0 )
		return OP_OK;
#else
	*file = fopen(filename, "rb");
	if( (*file) != NULL )
		return OP_OK;
#endif

	return OPEN_READ_ERROR;
}

enum operation_status open_binary_file_to_write( const char *filename, FILE** file) {
#ifdef _MSC_VER
	int32_t rc = fopen_s(file, filename, "wb");
	if (rc == 0 )
		return OP_OK;
#else
	*file = fopen(filename, "wb");
	if( (*file) != NULL )
		return OP_OK;
#endif
	
	return OPEN_WRITE_ERROR;
}

char *convert_err_code_to_string[MAX_ERROR_CODE + 1] =
{
	  "unknown",
	  "Open file for reading",
	  "Invalid BMP file signature",
	  "BMP file BitCount not supported",
	  "BMP file header",
	  "Reading data",
	  "File seek operation",
	  "Open file for writing",
	  "Writing data",
	  "memory allocation"
};

void  print_msg( FILE * const out_stream, const char *message, const char *argument_string,  int32_t *argument_int)
{  
	if(message == NULL)
		return;

#ifdef _MSC_VER
	char *PressEnterKey = "\nPress <Enter> key to continue.";
#else
	char *PressEnterKey = "\n";
#endif	

	// convert_err_code_to_string convention
	if (argument_int != NULL) {
		if (*argument_int > MAX_ERROR_CODE || *argument_int < 0)
			*argument_int = 0;
	}

	if (argument_string != NULL && argument_int == NULL) {
		fprintf(out_stream, "%s: %s%s", message, argument_string, PressEnterKey);
	}
	else if(argument_string != NULL && argument_int != NULL) { 
		fprintf(out_stream,  "%s: %s (%s)%s", message, argument_string, convert_err_code_to_string[*argument_int], PressEnterKey);
	}
	else if(argument_string == NULL && argument_int != NULL) { 
		fprintf(out_stream, "%s: (%s)%s", message, convert_err_code_to_string[*argument_int], PressEnterKey);
	}
	else {	
		fprintf(out_stream, "%s%s", message, PressEnterKey);
	}
		
#ifdef _MSC_VER 
	getchar();
#endif	
}

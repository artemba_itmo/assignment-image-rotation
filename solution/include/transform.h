#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "rawimage.h"
#include "util.h"


#define ROTATION_NONE   0
#define ROTATION_90CCW  1
#define ROTATION_90CW	2
#define ROTATION_180    3


struct image rotate(struct image const source_img, const int32_t rotation_direction);

int32_t alloc_image_buffer(struct image *imgptr);

void free_image_buffer(struct image *imgptr);

#endif

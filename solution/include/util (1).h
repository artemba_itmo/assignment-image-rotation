#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdint.h>

/*  deserializer   */
enum error_status {
	OP_OK = 0,
		OPEN_READ_ERROR,
		READ_INVALID_SIGNATURE,
		READ_INVALID_BITS,
		READ_INVALID_HEADER,
		READ_DATA_ERROR,
		READ_SKIP_ERROR,
		OPEN_WRITE_ERROR,
		WRITE_ERROR,
		MEMORY_ALLOC_ERROR
	/* коды других ошибок  */
};

enum error_status open_binary_file_to_read( char *filename, FILE** file);

enum error_status open_binary_file_to_write( char *filename, FILE** file);

void  print_msg(FILE *out_stream, char *message, char *argument_string, int32_t *argument_int);

char * convert_err_code_to_string(int32_t const argument_int);

#ifndef _MSC_VER 
#define sprintf_s snprintf
#endif

#endif




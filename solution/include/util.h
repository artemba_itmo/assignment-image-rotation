#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdint.h>

/*  deserializer   */
enum operation_status {
	OP_OK = 0,
	OPEN_READ_ERROR,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_DATA_ERROR,
	READ_SKIP_ERROR,
	OPEN_WRITE_ERROR,
	WRITE_ERROR,

	/* коды других ошибок  */
	MEMORY_ALLOC_ERROR
};

#define MAX_ERROR_CODE (MEMORY_ALLOC_ERROR)

enum operation_status open_binary_file_to_read( const char *filename, FILE** file);

enum operation_status open_binary_file_to_write( const char *filename, FILE** file);

void  print_msg( FILE * const out_stream, const char *message, const char *argument_string,  int32_t *argument_int);


#endif
